//
//  ViewController.swift
//  Demo
//
//  Created by Krowdforce on 05/03/19.
//  Copyright © 2019 Krowdforce. All rights reserved.
//

import UIKit

class tablecell: UITableViewCell {
    
    @IBOutlet weak var Labelname: UILabel!
    var model:Model! {
        didSet{
            Labelname?.text = model.name
        }
        
    }
    
}
class ViewController: UIViewController {

    @IBOutlet weak var tableviewObj: UITableView!
    var  Course = [Model]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(123)
        //print(12345678)
        
        
        
      let JsonUrl = "https://api.letsbuildthatapp.com/jsondecodable/courses_missing_fields"
        guard let url = URL(string: JsonUrl) else{
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else{return}
            
            do{
                let course = try JSONDecoder().decode([Model].self, from: data)
              //  print(course.name)
                self.Course = course
            }catch let Jsonerr {
                print("Error occur", Jsonerr)
                
            }
            DispatchQueue.main.async {
               self.tableviewObj.reloadData()
            }
            
        }.resume()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

}
extension ViewController:UITableViewDelegate,UITableViewDataSource{
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Course.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tablecell", for: indexPath) as! tablecell
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myvc = self.storyboard?.instantiateViewController(withIdentifier: "NextViewController") as! NextViewController
        self.navigationController?.pushViewController(myvc, animated: true)
    }
}

struct Model:Decodable {
    let id: Int?
    let name:String
    
}
